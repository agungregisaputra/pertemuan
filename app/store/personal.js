Ext.define('Latihan.store.Personal', {
    extend: 'Ext.data.Store',
    storeId: 'personal',
    alias: 'store.Personal',

    fields: [
       'photo', 'nama', 'status'
    ],

    data: { items: [
        { photo: '<img src="resources/image/agung.jpg" width="100px" height="100px">', nama: 'Agung Regi Saputra', status: 'Mahasiswa'},
        { photo: '<img src="resources/image/assasin.jpg" width="100px" height="100px">', nama: 'Assassins Creed Odyssey', genre: 'Action', status: 'PC'},
        { photo: '<img src="resources/image/pubg.jpg" width: "100px" height = "100px">', nama: 'PUBGM (PLAYER UNKNOWN BATTLEGROUNDS MOBILE)', genre: 'FPS', status: 'Android'},
        { photo: '<img src="resources/image/valorant.jpg" width: "100px" height = "100px">', nama: 'VALORANT', genre: 'FPS', status: 'PC'}
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});