Ext.define('Latihan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
         'photo', 'npm', 'nama', 'email', 'phone', 'birthday'
    ],

    data: { items: [
        {photo: '<img src="resources/image/agung.jpg" width="100px" height="100px">' ,npm: '183510009', nama: 'Agung regi saputra', email: "agungregisaputra@student.uir.ac.id", phone: "085272820602", birthday: "1997-22-02" },
        {photo: '<img src="resources/image/andi.jpeg" width="100px" height="100px">' ,npm: '183510030', nama: 'andi azwinnur',  email: "andiazwinnur@student.uir.ac.id",  phone: "078068068877", birthday: "1998-29-01" },
        {photo: '<img src="resources/image/hery.jpeg" width="100px" height="100px">' ,npm: '183510006', nama: 'Hery Hermawan', email: "heryhermawan@student.uir.ac.id", phone: "082233493936", birthday: "1999-18-09"},
        {photo: '<img src="resources/image/Heru.jpeg" width="100px" height="100px">' ,npm: '183510080', nama: 'Heru Rifai',  email: "herufai@student.uir.ac.id",       phone: "56546576576", birthday: "1991-11-09" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
