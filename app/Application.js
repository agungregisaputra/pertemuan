/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Latihan.Application', {
    extend: 'Ext.app.Application',

    name: 'Latihan',

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    stores: [
        // TODO: add global / shared stores here
    ],

    launch: function () {
        var LoggedIn;
        LoggedIn = localStorage.getItem("LoggedIn");
        if (!LoggedIn) {
            this.overlay = Ext.Viewport.add({
                xtype: 'login',
                floated: true,
                showAnimation: {
                    type: 'popIn',
                    duration: 250,
                    easing: 'ease-out'
                },
                hideAnimation: {
                    type: 'popOut',
                    duration: 250,
                    easing: 'ease-out'
                },
                centered: false,
                width: "100%", 
                height: "100%",

                scrollable: true
            });
        }

        this.overlay.show();
    }, 

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
