Ext.define('Latihan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype : 'xdata',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Latihan.store.Personnel',
        'Ext.field.Search'
    ],
      viewModel: {
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
        docked: 'top',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'searchfield',
                placeHolder: 'Search',
                name: 'searchfield',
                listeners: {
                    change: function(me, newValue, oldValue, e0pts){
                        personnelStore = Ext.getStore('personnel');
                        personnelStore.filter('nama', newValue);
                    }
                }
            }
        ]
    },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'ks-basic demo-solid-background',
        itemTpl: '{photo}<br>{nama}<br><font color="blue">{email}</font color><br>{phone}<hr>',
        bind: {
            store: '{personnel}',
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>NPM: </td><td>{npm}</td></tr>' +
                    '<tr><td>NAMA:</td><td>{nama}</td></tr>' + 
                    '<tr><td>Email:</td><td>{email}</div></td></tr>' +
                    '<tr><td>PHONE:</td><td>{phone}</td></tr>'
        }
    }]
});