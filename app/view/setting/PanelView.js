Ext.define('Latihan.view.setting.PanelView', {
    extend: 'Ext.Container',
    xtype: 'spanel',
    id: 'spanel',
    layout: 'card',
    items: [
        {
            xtype: 'panel',
            title: 'Panel 1',
            items: [
                {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    scrollable: {
                        y: false
                    },
                    items: [
                        {
                            xtype: 'spacer'
                        },
                        {
                            xtype: 'button',
                            text: 'Next',
                            ui: 'action',
                            handler: function () {
                                Ext.getCmp('spanel').setActiveItem(1);
                            }
                        }

                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            title: 'Panel 2'
        }
    ]
});