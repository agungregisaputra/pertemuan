Ext.define('Latihan.view.tree.TreePanel', {
    extend: 'Ext.Container',
    xtype: 'tree-panel',
    requires: [
        'Ext.layout.HBox',
        'Latihan.view.tree.TreeList'
    ],
    
    layout: {
        type: 'hbox',
        pack: 'center', 
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodypadding: 10
    },
    items: [
        {
            xtype: 'tree-list',
            flex: 1
        },
        {
            xtype: 'panel',
            id: 'detailtree',
            flex: 1,
            items: [{
                xtype: 'xdata'
            }]
        }
    ]
})