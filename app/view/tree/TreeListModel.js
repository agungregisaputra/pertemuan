Ext.define('Latihan.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var status = get('treelist.status'),
                path;
            if (status) {
                path = status.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected'; 
            }
        }
    }, 

    stores: {
        personal: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'All',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    text: 'Android',
                    iconCls: 'x-fa fa-mobile',
                    leaf: true
                    },{
                        text: 'PC',
                        iconCls: 'x-fa fa-desktop',
                        leaf: true
                    }
                ]
            }
        }
    }
});