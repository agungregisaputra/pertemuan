Ext.define('Latihan.view.tree.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection',
        'Latihan.view.tree.TreeListModel'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{populer}',

    listeners: {
        itemtap: function(me, index, target, record, e, e0pts){
           populerStore = Ext.getStore('personal');
           populerStore.filter('status', record.data.text);
        }
    }
});