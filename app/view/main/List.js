/**
 * This view is an example list of people.
 */
Ext.define('Latihan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Latihan.store.Personnel'
    ],

    title: 'DATA MAHASISWA TEKNIK INFORMATIKA',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Npm',  dataIndex: 'npm', width: 110 },
        { text: 'Name',  dataIndex: 'nama', width: 150 },
        { text: 'Email', dataIndex: 'email', width: 280 },
        { text: 'Phone', dataIndex: 'phone', width: 150 },
        { text: 'Birthday', dataIndex: 'birthday', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
