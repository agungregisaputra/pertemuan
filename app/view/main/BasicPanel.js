Ext.define('Latihan.view.main.BasicPanel', {
    extend: 'Ext.Container',
    xtype: 'BasicPanel',

    requires: 'Ext.layout.VBox',

    layout: {
        type: 'vbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodyPadding: 10
    },
    items: [
        {
            layout: {
                type: 'hbox',
                pack: 'center',
                align: 'stretch'
            },
            defaults: {
                flex: 1,
                bodyPadding: 10,    
            },
            items: [
                {
                    xtype: 'panel',
                    margin: '0 5 0 0'
                },
                {
                    xtype: 'panel',
                    title: 'Title',
                    margin: '0 0 0 5'
                }
            ]
        },
        {
            xtype: 'panel',
            title: 'Built in Tools',
            html: 'Lorem ipsum',
            tools: [
                {
                    type: 'minimize' 
                },
                {
                    type: 'refresh' 
                },
                {
                    type: 'search' 
                },
                {
                    type: 'save' 
                },
                {
                    type: 'menu' 
                }
            ]
            
        },

        {
            xtype: 'panel',
            title: 'Built in Tools',
            html: 'Lorem ipsum',
            tools: [
                {iconCls: 'x-fa fa-wrench'},
                {iconCls: 'x-fa fa-reply'},
                {iconCls: 'x-fa fa-reply-all'},
                {iconCls: 'x-fa fa-rocket'}
            ]
            
        },
    ]
},
);