Ext.define('Latihan.view.main.Carousel', {
    extend: 'Ext.Container',
    xtype : 'xcarousel',
    requires: [
        'Ext.carousel.Carousel'
    ],

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    items: [{
        xtype: 'carousel',
        items: [{
            html: '<p>Pertama</p>',
            cls: 'card'
        },
        {
            html: '<p>kedua</p>',
            cls: 'card'
        },
        {
            html: 'ketiga',
            cls: 'card'
        }]
    }, {
        xtype: 'carousel',
        ui: 'light',
        direction: 'vertical',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
            cls: 'card dark'
        },
        {
            html: 'And can also use <code>ui:light</code>.',
            cls: 'card dark'
        },
        {
            html: 'Card #3',
            cls: 'card dark'
        }]
    }]
});