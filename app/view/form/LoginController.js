Ext.define('Latihan.view.form.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    onLogin: function() {
        var form = this.getView();
        var username = form.getFields('username').getValue();
        var password = form.getFields('password').getValue();
        if(username && password){
        	if(username=="agung" && password=="agung123"){
        		localStorage.setItem('logeddin', true);
        		Ext.Msg.alert("Login Berhasil");
        		form.hide();
        	}
        	else{
        		Ext.Msg.alert("Login Gagal");
        	}
        }
    }
});