Ext.define('Latihan.view.form.User', {
    extend: 'Ext.form.Panel',
    xtype : 'xuser',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'basicform',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'Personal Info',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'npm',
                    label: 'NPM',
                    placeHolder: '183510009',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'nama',
                    label: 'Nama',
                    placeHolder: 'Agung Regi Saputra',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'selectfield',
                    name: 'kelas',
                    label: 'Kelas',
                    options: [
                        {
                            text: 'V A',
                            value: 'V A'
                        },
                        {
                            text: 'V B',
                            value: 'V B'
                        },
                        {
                           text: 'V C',
                            value: 'V C'
                        },
                        {
                           text: 'V D',
                            value: 'V D'
                        },
                        {
                           text: 'V E',
                            value: 'V E'
                        },
                        {
                           text: 'V F',
                            value: 'V F'
                        },
                        {
                           text: 'V G',
                            value: 'V G'
                        }
                        ]
                    },
                        {
                            xtype: 'textareafield',
                            name: 'alamat',
                            id: 'alamat',
                            label: 'Alamat',
                            placeHolder: 'Jln. Barongsai',
                            //autoCapitalize: true,
                            required: true,
                            clearIcon: true
                        },
                    ]
                },
            {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'SAVE',
                    ui: 'action',
                    scope: this,
                    hasDisabled: false,
                    handler: function(btn){
                        Ext.Msg.alert('Data Tersimpan',' Data Tersimpan', Ext.emptyFn);
                        }
                },
                {
                    text: 'Reset',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                }
            ]
        }
        ]},
);